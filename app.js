const express = require(`express`)
const port = `4000`
var app = express();

app.get(`/second`, function(req, res) {
    var result = JSON.stringify({status:"success", message:"Second Node JS Application"})
    res.send(result)
})

app.listen(port, function(){
    console.log(`Server is now running in the port:${port}`)
})